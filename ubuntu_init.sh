#! /bin/bash
set -euo pipefail

# Add user and grant privileges. 
echo New user:
read USERNAME
if id -u $USERNAME &> /dev/null; then
    echo User exists.
    exit 0
fi
useradd --create-home --shell "/bin/bash" --groups sudo "${USERNAME}"
echo User added.

# Copy over the root user's `authorize_keys` files to the new sudo user
echo "Copy root's authorized keys file? (y)es (n)no"
read COPY_AUTHORIZED_KEYS

# Check whether the root account has a real password set.
encrypted_root_pw="$(grep root /etc/shadow | cut --delimiter=: --fields=2)"

if [ "${encrypted_root_pw}" != "*" ]; then
    # Transfer auto-generated root password to user if present
    # and lock the root account to password-based access
    echo "${USERNAME}:${encrypted_root_pw}" | chpasswd --encrypted
    passwd --lock root
else
    # Delete invalid password for user if using keys so that a new password
    # can be set without providing a previous value.
    passwd --delete "${USERNAME}"
fi

# Expire the sudo user's password immediately to force a change.
chage --lastday 0 "${USERNAME}"

# Create SSH directory for sudo user.
echo "Creating user's SSH directory."
home_directory="$(eval echo ~${USERNAME})"
mkdir --parents "${home_directory}/.ssh"

# Copy `authorized_keys` file form root if requested
if [ "${COPY_AUTHORIZED_KEYS}" = "y" ]; then
    echo "Moving authorize keys to the user's SSH directory."
    cp /root/.ssh/authorized_keys "${home_directory}/.ssh"
fi

# Adjust SSH files ownership and permissions
chmod 0700 "${home_directory}/.ssh"
chmod 0600 "${home_directory}/.ssh/authorized_keys"
chown --recursive "${USERNAME}":"${USERNAME}" "${home_directory}/.ssh"

# Port to use in SSH
echo Configuring SSH.
echo SSH Port:
read PORT

# Configure SSH
sed --in-place "s/^PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config
sed --in-place "s/#Port.*/Port ${PORT}/g" /etc/ssh/sshd_config
if sshd -t -q; then
    systemctl restart sshd
fi

# Add exception for SSH and then enable UFW firewall
echo Enabling UFW.
ufw allow $PORT
ufw --force enable